package com.aastudio.hotballnew.utils;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.aastudio.hotballnew.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static com.aastudio.hotballnew.utils.Utils.log;
import static com.aastudio.hotballnew.utils.Utils.logDebug;

public class Loading extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        logDebug(Loading.class, "onCreate");
        // Authentication
        // Initialize Firebase Auth
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser == null) {
            log(Loading.class, "User is not signed in.");
            signIn();
        } else {
            log(Loading.class, "User is signed in.");
            // TODO: go to main menu
//            Intent menuIntent = new Intent(Menu.java);
//            startActivity(menuIntent)
        }
    }

    private void signIn() {
        /* TODO: make ui visible
         *   check for create account/sign in button
         *   check for correct input
         *   firebase sign in*/
    }
}
