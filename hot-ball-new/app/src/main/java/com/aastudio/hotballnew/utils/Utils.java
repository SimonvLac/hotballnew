package com.aastudio.hotballnew.utils;

import android.util.Log;

class Utils {

    /**
     * @param c Class used to locate log call
     * @param message message meant to send to terminal
     */
    static void log(Class c, String message) {
        Log.d(Variables.BASE_TAG + c.getSimpleName(), message);
    }

    /**
     * @param c Class used to locate log call
     * @param message message meant to send to terminal
     * used for only debugging messages
     */
    static  void  logDebug(Class c, String message) {
        if (Variables.DEBUG)
            log(c,message);
    }
}
